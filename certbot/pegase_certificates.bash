#!/bin/bash

domain=pegase.mobilizing-js.net

domains="${domain}"

for sub_domain in {a..z} www; do
    domains+=",${sub_domain}.${domain}"
done

echo
echo "domains: "
echo
echo "${domains}"