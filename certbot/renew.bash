#!/bin/bash

if [[ $(hostname) != vps94214 ]] ; then
    echo '##########################################'
    echo '# Please run on mobilizing-js.net server #'
    echo '##########################################'
    echo

    exit 1
fi

echo '#########################################'
echo '# Please select Apach Web Server plugin #'
echo '#########################################'
echo

sudo certbot certonly -d 'pegase.mobilizing-js.net,a.pegase.mobilizing-js.net,b.pegase.mobilizing-js.net,c.pegase.mobilizing-js.net,d.pegase.mobilizing-js.net,e.pegase.mobilizing-js.net,f.pegase.mobilizing-js.net,g.pegase.mobilizing-js.net,h.pegase.mobilizing-js.net,i.pegase.mobilizing-js.net,j.pegase.mobilizing-js.net,k.pegase.mobilizing-js.net,l.pegase.mobilizing-js.net,m.pegase.mobilizing-js.net,n.pegase.mobilizing-js.net,o.pegase.mobilizing-js.net,p.pegase.mobilizing-js.net,q.pegase.mobilizing-js.net,r.pegase.mobilizing-js.net,s.pegase.mobilizing-js.net,t.pegase.mobilizing-js.net,u.pegase.mobilizing-js.net,v.pegase.mobilizing-js.net,w.pegase.mobilizing-js.net,x.pegase.mobilizing-js.net,y.pegase.mobilizing-js.net,z.pegase.mobilizing-js.net,www.pegase.mobilizing-js.net'  --cert-path "$(pwd)" --key-path "$(pwd)" --fullchain-path "$(pwd)" --chain-path "$(pwd)" --work-dir "$(pwd)" --logs-dir "$(pwd)/log"  --config-dir "$(pwd)" --register-unsafely-without-email


