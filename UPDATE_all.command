#!/bin/bash

# double-click
cd "$( dirname "$0" )" || (echo "no dir: ${0}"; exit 1)

# for now, track develop branch
git_branch='develop'

git checkout "$git_branch"
git pull --strategy=recursive --strategy-option=theirs origin "$git_branch"


# update submodules
git update --init --recursive

# update DNS
(cd dns && bash init_dependencies.bash)

# update node_modules

# qrcode, http-server
npm install

# TODO: add science
for module in intro feminisme paix ; do
  (cd "$module" && npm install)
done
