#!/bin/bash



npx qrcode --width 512 --output qrcode_wifi_pegase_a.png 'WIFI:S:Pégase-A;T:WPA;P:12345678;;'
npx qrcode --width 512 --output qrcode_wifi_pegase_b.png 'WIFI:S:Pégase-B;T:WPA;P:12345678;;'
npx qrcode --width 512 --output qrcode_wifi_pegase_c.png 'WIFI:S:Pégase-C;T:WPA;P:12345678;;'


npx qrcode --width 512 --output qrcode_url_a_8000.png 'https://a.pegase.mobilizing-js.net:8000'
npx qrcode --width 512 --output qrcode_url_a_8001.png 'https://a.pegase.mobilizing-js.net:8001'
npx qrcode --width 512 --output qrcode_url_a_8002.png 'https://a.pegase.mobilizing-js.net:8002'
npx qrcode --width 512 --output qrcode_url_a_8003.png 'https://a.pegase.mobilizing-js.net:8003'
npx qrcode --width 512 --output qrcode_url_a_8004.png 'https://a.pegase.mobilizing-js.net:8004'

npx qrcode --width 512 --output qrcode_url_b_8000.png 'https://b.pegase.mobilizing-js.net:8000'
npx qrcode --width 512 --output qrcode_url_b_8001.png 'https://b.pegase.mobilizing-js.net:8001'
npx qrcode --width 512 --output qrcode_url_b_8002.png 'https://b.pegase.mobilizing-js.net:8002'
npx qrcode --width 512 --output qrcode_url_b_8003.png 'https://b.pegase.mobilizing-js.net:8003'
npx qrcode --width 512 --output qrcode_url_b_8004.png 'https://b.pegase.mobilizing-js.net:8004'

npx qrcode --width 512 --output qrcode_url_c_8000.png 'https://c.pegase.mobilizing-js.net:8000'
npx qrcode --width 512 --output qrcode_url_c_8001.png 'https://c.pegase.mobilizing-js.net:8001'
npx qrcode --width 512 --output qrcode_url_c_8002.png 'https://c.pegase.mobilizing-js.net:8002'
npx qrcode --width 512 --output qrcode_url_c_8003.png 'https://c.pegase.mobilizing-js.net:8003'
npx qrcode --width 512 --output qrcode_url_c_8004.png 'https://c.pegase.mobilizing-js.net:8004'
