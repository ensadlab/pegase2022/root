#!/bin/bash

# double-click
cd "$( dirname "$0" )" || (echo "no dir: ${0}"; exit 1)

open -a Terminal 'dns/DNS_run_local.command'
open -a Terminal 'dns/DNS_run_public.command'

open -a Terminal 'intro/SERVER_run_port_8000.command'
open -a Terminal 'feminisme/SERVER_run_port_8001.command'
open -a Terminal 'paix/SERVER_run_port_8002.command'
open -a Terminal 'science/SERVER_run_port_8003.command'
