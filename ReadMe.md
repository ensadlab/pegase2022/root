# Installation

Clone this repository

> git clone

Then init all dependencies with the script `UPDATE_all.command`.

It will:
- update and init all git sublodules
 > git update --init --recursive
- compile `dnsmasq` in `dns` directiory
- install node modules in node projects

Note: Run this script again when updating `dnsmasq` or any node project.

# Configure your network preferences

`dnsmasq`is used as as local DNS server to attributes IP to devices from defined URL. Certificates are generated with certbot to allow https access, which is required for embedded sensors usage on mobile devices.

## Run a server instance

On your computer, choose the network interface you want to run your server from.
For this exemple, we'll use this settings :
- the Mobilizing projects are served from Wi-Fi
- the Internet is connected via Ethernet

Create a specific network configuration to get it up and running easily after setup, ex : `Mobilizing Server 10.10.0.1`

Choose Ethernet, that will serve the projects from a specific domain name, and use these settings :

- Choose IP **manual settings** with
- IP : 10.10.0.1
- Submask : 255.255.0.0
- Router : 10.10.0.1
- DNS Server : 10.10.0.1

Then, choose Wi-Fi, that will connect to your home Internet connection router and set these setting :

- Choose IP through DHCP (as usual with your Internet Box)
- DNS Server : 127.0.0.1

This will allow a loop back through dnsmask, that will respond to `pegase.mobilizing-js.net`domain without breaking your Internet connection.

From your other devices, connect on your Wi-Fi network and simply use the project URL to access to it, like : `https://a.pegase.mobilizing-js.net:8000`

