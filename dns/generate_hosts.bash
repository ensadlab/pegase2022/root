#!/bin/bash

domain_short="pegase"
domain="${domain_short}.mobilizing-js.net"

{
    echo "# generated hosts file";
    echo "";
    echo "10.10.0.1                ${domain}";
    echo "10.10.0.1                www.${domain}";
    echo "";
} > hosts

letter_ip=1
for sub_domain in {a..z} ; do
    echo "10.10.0.${letter_ip}                ${sub_domain}.${domain}" >> hosts
    echo "10.10.0.${letter_ip}                ${domain_short}-${sub_domain}" >> hosts
    (( letter_ip++ ))
done

echo >> hosts
