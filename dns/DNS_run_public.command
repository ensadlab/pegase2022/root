#!/bin/bash

# double-click
cd "$( dirname "$0" )" || (echo "no dir: ${0}"; exit 1)

bash DNS_run.bash './dnsmasq_public.conf'
